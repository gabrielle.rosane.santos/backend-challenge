# Validador de senhas


## Description

Api java para validação de senha com os seguintes requisitos:

*   Nove ou mais caracteres
*   Ao menos 1 dígito
*   Ao menos 1 letra minúscula
*   Ao menos 1 letra maiúscula
*   Ao menos 1 caractere especial
*   Não possuir caracteres repetidos dentro do conjunto

## 

## Building locally


```
mvn clean install
cd target 
java -jar <jar file name>

```
## Endpoints


* POST localhost:8080/api/password-validate

  {
  "password": "AbTp9+-fok"
  }



## Structure

*  Chain of Responsibility onde cada chain ficou responsável por validar um requisito da descrição.
* Foi usada uma expressão regular em cada chain para validar se a senha possui o requisito;











