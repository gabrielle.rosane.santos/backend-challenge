package com.backendchallenge.component.chain;

import com.backendchallenge.context.PasswordValidationContext;
import com.backendchallenge.model.dto.ValidationDTO;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@Component
@Primary
public class BaseValidatorPasswordChain {

    @Getter
    private BaseValidatorPasswordChain next;

    public Optional<ValidationDTO> build(String password, PasswordValidationContext context) {

        var passwordWithoutSpaces = StringUtils.deleteWhitespace(password);

        var validationDTO = getValidation(passwordWithoutSpaces);

        if (validationDTO != null){
            context.getValidations().add(validationDTO);
        }

        if (next != null) {
            return next.build(passwordWithoutSpaces, context);
        }

        return Optional.ofNullable(context.getValidations().stream()
                .reduce(ValidationDTO.builder()
                        .validations(new ArrayList<>())
                        .build(), (acc, el) -> {
                    var validationsMessage = acc.getValidations();
                    validationsMessage.add(el.getValidation());
                    acc.setValidations(validationsMessage);
                    acc.setValid(Boolean.FALSE);
                    return acc;
                }));
    }

    protected ValidationDTO getValidation(String password) {
        return null;
    }

    public BaseValidatorPasswordChain chain(BaseValidatorPasswordChain next) {
        if (this.next == null) {
            this.next = next;
            return this;
        } else {
            return this.next.chain(next);
        }
    }

}
