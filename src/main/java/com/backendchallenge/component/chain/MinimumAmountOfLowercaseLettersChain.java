package com.backendchallenge.component.chain;

import com.backendchallenge.model.dto.MessageError;
import com.backendchallenge.model.dto.ValidationDTO;
import org.springframework.stereotype.Component;

@Component
public class MinimumAmountOfLowercaseLettersChain extends BaseValidatorPasswordChain {

    private static final String pattern = "(.*[a-z].*)";

    @Override
    protected ValidationDTO getValidation(String password) {

        if (!password.matches(pattern)) {
            return ValidationDTO.builder()
                    .validation(MessageError.MINIMUM_AMOUNT_OF_LOWER_CASES_LETTERS.getMensagem())
                    .build();
        }
        return null;
    }
}
