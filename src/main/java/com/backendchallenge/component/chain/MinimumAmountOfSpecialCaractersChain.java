package com.backendchallenge.component.chain;

import com.backendchallenge.model.dto.MessageError;
import com.backendchallenge.model.dto.ValidationDTO;
import org.springframework.stereotype.Component;

@Component
public class MinimumAmountOfSpecialCaractersChain extends BaseValidatorPasswordChain {

    private static final String pattern = "(.*[!@#$%^&*()+-].*)";

    @Override
    protected ValidationDTO getValidation(String password) {
        if (!password.matches(pattern)) {
            return ValidationDTO.builder()
                    .validation(MessageError.MINIMUM_AMOUNT_OF_SPECIAL_CARACTERS.getMensagem())
                    .build();
        }
        return null;
    }
}
