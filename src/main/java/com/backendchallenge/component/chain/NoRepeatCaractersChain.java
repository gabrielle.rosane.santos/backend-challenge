package com.backendchallenge.component.chain;

import com.backendchallenge.model.dto.MessageError;
import com.backendchallenge.model.dto.ValidationDTO;
import org.springframework.stereotype.Component;

@Component
public class NoRepeatCaractersChain extends BaseValidatorPasswordChain {

    private static final String pattern = "([a-zA-Z0-9!@#$%^&*()+-])*.*\\1";

    @Override
    protected ValidationDTO getValidation(String password) {

        if (password.matches(pattern)) {
            return ValidationDTO.builder()
                    .validation(MessageError.NO_REPEAT_CARACTERS.getMensagem())
                    .build();
        }
        return null;
    }
}
