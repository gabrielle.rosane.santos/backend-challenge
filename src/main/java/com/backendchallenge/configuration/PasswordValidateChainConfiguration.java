package com.backendchallenge.configuration;

import com.backendchallenge.component.chain.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PasswordValidateChainConfiguration {

    @Bean("validatorPasswordChain")
    public BaseValidatorPasswordChain getPasswordValidateChain(BaseValidatorPasswordChain chain,
                                                               MinimumAmountOfCharactersChain c1,
                                                               MinimumAmountOfDigitsChain c2,
                                                               MinimumAmountOfLowercaseLettersChain c3,
                                                               MinimumAmountOfSpecialCaractersChain c4,
                                                               MinimumAmountOfUppercaseLettersChain c5,
                                                               NoRepeatCaractersChain c6) {
        chain
                .chain(c1)
                .chain(c2)
                .chain(c3)
                .chain(c4)
                .chain(c5)
                .chain(c6);
        return chain;
    }

}
