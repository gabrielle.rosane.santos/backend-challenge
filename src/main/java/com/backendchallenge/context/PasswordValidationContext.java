package com.backendchallenge.context;
import com.backendchallenge.model.dto.ValidationDTO;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class PasswordValidationContext {

    @Builder.Default
    private List<ValidationDTO> validations = new ArrayList<>();
}
