package com.backendchallenge.controller;

import com.backendchallenge.model.dto.PasswordDTO;
import com.backendchallenge.model.dto.ValidationDTO;
import com.backendchallenge.service.PasswordServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
public class PasswordController {

    private final PasswordServiceImpl passwordService;

    @PostMapping()
    ValidationDTO validatePassword(@RequestBody @Valid PasswordDTO passwordDTO) {
        return passwordService.validatePassword(passwordDTO);
    }
}
