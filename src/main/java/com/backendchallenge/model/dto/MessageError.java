package com.backendchallenge.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MessageError {

    MINIMUM_AMOUNT_OF_CHARACTERS("É necessário mais de 9 caracteres!"),
    MINIMUM_AMOUNT_OF_DIGITS("É necessário ter ao menos 1 dígito!"),
    MINIMUM_AMOUNT_OF_LOWER_CASES_LETTERS("É necessário ter ao menos 1 letra minúscula!"),
    MINIMUM_AMOUNT_OF_SPECIAL_CARACTERS("É necessário ter ao menos 1 caractere especial!"),
    MINIMUM_AMOUNT_OF_UPPER_CASE_LETTERS("É necessário ter ao menos 1 letra maiúscula!"),
    NO_REPEAT_CARACTERS( "Caracteres Repetidos não são aceitos !");

    private String mensagem;
}
