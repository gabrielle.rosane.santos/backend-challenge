package com.backendchallenge.model.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PasswordDTO {

    @NotNull
    private String password;
}
