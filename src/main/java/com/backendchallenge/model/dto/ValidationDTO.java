package com.backendchallenge.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ValidationDTO {

    @Builder.Default
    private boolean isValid = true;

    @JsonIgnore
    private String validation;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> validations;

}
