package com.backendchallenge.service;

import com.backendchallenge.model.dto.PasswordDTO;
import com.backendchallenge.model.dto.ValidationDTO;

@FunctionalInterface
public interface PasswordService {
    ValidationDTO validatePassword(PasswordDTO passwordDTO);
}
