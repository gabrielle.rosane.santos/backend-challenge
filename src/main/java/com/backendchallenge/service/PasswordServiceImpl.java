package com.backendchallenge.service;

import com.backendchallenge.component.chain.BaseValidatorPasswordChain;
import com.backendchallenge.context.PasswordValidationContext;
import com.backendchallenge.model.dto.PasswordDTO;
import com.backendchallenge.model.dto.ValidationDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class PasswordServiceImpl implements PasswordService {

    private final BaseValidatorPasswordChain validatorPasswordChain;

    public ValidationDTO validatePassword(PasswordDTO passwordDTO) {
        return validatorPasswordChain.build(passwordDTO.getPassword(), PasswordValidationContext.builder().build())
                .orElse(ValidationDTO.builder().isValid(true).build());
    }
}
