package com.backendchallenge.component.chain;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class BaseValidatorPasswordChainTest {

    @InjectMocks
    protected BaseValidatorPasswordChain chain;

    public void setup() {
        ReflectionTestUtils.setField(chain, "next", null);
    }

}
