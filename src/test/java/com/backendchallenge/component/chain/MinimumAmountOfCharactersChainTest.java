package com.backendchallenge.component.chain;

import com.backendchallenge.context.PasswordValidationContext;
import com.backendchallenge.model.dto.MessageError;
import com.backendchallenge.model.dto.ValidationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class MinimumAmountOfCharactersChainTest extends BaseValidatorPasswordChainTest {

    private static final String PASSWORD_VALID = "123456789";
    private static final String PASSWORD_INVALID = "12345678";

    @InjectMocks
    private MinimumAmountOfCharactersChain minimumAmountOfCharactersChain;

    @BeforeEach
    void setUp() {
        super.setup();
        chain.chain(minimumAmountOfCharactersChain);
    }

    @Test
    void testInputInvalidPassword() {
        Optional<ValidationDTO> validationDTO = chain.build(PASSWORD_INVALID, PasswordValidationContext.builder().build());
        assertTrue(validationDTO.isPresent());
        assert (validationDTO.isPresent());
        assertEquals(1, validationDTO.get().getValidations().size());
        assertEquals(MessageError.MINIMUM_AMOUNT_OF_CHARACTERS.getMensagem(), validationDTO.get().getValidations().get(0));
    }

    @Test
    void testInputValidPassword() {
        Optional<ValidationDTO> validationDTO = chain.build(PASSWORD_VALID, PasswordValidationContext.builder().build());
        assertTrue(validationDTO.isPresent());
        assert (validationDTO.isPresent());
        assertEquals(0, validationDTO.get().getValidations().size());
    }


}
