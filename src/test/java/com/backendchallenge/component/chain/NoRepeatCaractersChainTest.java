package com.backendchallenge.component.chain;

import com.backendchallenge.context.PasswordValidationContext;
import com.backendchallenge.model.dto.MessageError;
import com.backendchallenge.model.dto.ValidationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class NoRepeatCaractersChainTest extends BaseValidatorPasswordChainTest {

    @InjectMocks
    protected BaseValidatorPasswordChain chain;

    public void setup() {
        ReflectionTestUtils.setField(chain, "next", null);
    }

    private static final String PASSWORD_VALID = "abcdef!";
    private static final String PASSWORD_INVALID = "gagaga!!";

    @InjectMocks
    private NoRepeatCaractersChain noRepeatCaractersChain;

    @BeforeEach
    void setUp() {
        super.setup();
        chain.chain(noRepeatCaractersChain);
    }

    @Test
    void testInputInvalidPassword() {
        Optional<ValidationDTO> validationDTO = chain.build(PASSWORD_INVALID, PasswordValidationContext.builder().build());
        assertTrue(validationDTO.isPresent());
        assert (validationDTO.isPresent());
        assertEquals(1, validationDTO.get().getValidations().size());
        assertEquals(MessageError.NO_REPEAT_CARACTERS.getMensagem(), validationDTO.get().getValidations().get(0));
    }

    @Test
    void testInputValidPassword() {
        Optional<ValidationDTO> validationDTO = chain.build(PASSWORD_VALID, PasswordValidationContext.builder().build());
        assertTrue(validationDTO.isPresent());
        assert (validationDTO.isPresent());
        assertEquals(0, validationDTO.get().getValidations().size());
    }
}
