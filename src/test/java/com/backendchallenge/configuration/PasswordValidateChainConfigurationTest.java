package com.backendchallenge.configuration;


import com.backendchallenge.component.chain.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PasswordValidateChainConfigurationTest {

    @InjectMocks
    private PasswordValidateChainConfiguration target;

    private BaseValidatorPasswordChain chain = new BaseValidatorPasswordChain();
    MinimumAmountOfCharactersChain c1 = new MinimumAmountOfCharactersChain();
    MinimumAmountOfDigitsChain c2 = new MinimumAmountOfDigitsChain();
    MinimumAmountOfLowercaseLettersChain c3 = new MinimumAmountOfLowercaseLettersChain();
    MinimumAmountOfSpecialCaractersChain c4 = new MinimumAmountOfSpecialCaractersChain();
    MinimumAmountOfUppercaseLettersChain c5 = new MinimumAmountOfUppercaseLettersChain();
    NoRepeatCaractersChain c6 = new NoRepeatCaractersChain();


    @Test
    void testChainConfig() {
        var validatorPasswordChain = target.getPasswordValidateChain(chain, c1, c2, c3, c4, c5, c6);

        assertEquals(chain, validatorPasswordChain);
        assertEquals(c1, chain.getNext());
        assertEquals(c2, c1.getNext());
        assertEquals(c3, c2.getNext());
        assertEquals(c4, c3.getNext());
        assertEquals(c5, c4.getNext());
        assertEquals(c6, c5.getNext());
    }
}
